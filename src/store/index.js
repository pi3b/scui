/**
 * @description 自动import导入所有 vuex 模块
 */

import { createStore } from 'vuex';

const files = import.meta.glob('./modules/*.js')
const modules = {}
Object.keys(files).forEach((key) => {
	modules[key.replace(/(\.\/|\.js)/g, '')] = files[key]
})

export default createStore({
	modules
});
